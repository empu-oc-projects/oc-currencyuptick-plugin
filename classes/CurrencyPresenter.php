<?php

namespace Empu\CurrencyUptick\Classes;

use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Request;
use October\Rain\Support\Collection;
use Responsiv\Currency\Classes\Converter as ResponsivConverter;
use Responsiv\Currency\Models\Currency as ResponsivCurrency;

class CurrencyPresenter
{
    const COOKIE_KEY = 'active_currency';

    /**
     * Cookie jar
     *
     * @var \Illuminate\Cookie\CookieJar
     */
    protected $cookieJar;

    /**
     * Request
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    protected $baseCurrency;

    protected $activeCurrency;

    protected $foreignCurrencies;

    public function __construct(CookieJar $cookieJar, Request $request)
    {
        $this->cookieJar = $cookieJar;
        $this->request = $request;

        $this->init();
    }

    public function init()
    {
        $this->baseCurrency = ResponsivCurrency::getPrimary()->currency_code;
        $activeCurrency = $this->request
            ->cookies
            ->get(self::COOKIE_KEY, $this->baseCurrency);
        
        $this->setActiveCurrency($activeCurrency);
    }

    public function instance()
    {
        return $this;
    }

    public function getBaseCurrency(): string
    {
        return $this->baseCurrency;
    }

    public function setActiveCurrency(string $currency): void
    {
        $this->activeCurrency = $currency;
    }

    public function getActiveCurrency(): string
    {
        return $this->activeCurrency;
    }

    public function storeActiveCurrency(string $activeCurrency = null): void
    {
        $activeCurrency = $activeCurrency ?: $this->activeCurrency;
        $cookie = $this->cookieJar->forever(self::COOKIE_KEY, $activeCurrency);

        $this->cookieJar->queue($cookie);
    }

    public function getForeignCurrencies(): Collection
    {
        $this->prepareForeignCurrencies();
        
        return $this->foreignCurrencies;
    }

    protected function prepareForeignCurrencies()
    {
        if (! $this->foreignCurrencies || $this->foreignCurrencies->isEmpty()) {
            $this->foreignCurrencies = $this->getEnabledCurrencies();
        }
    }

    public function getEnabledCurrencies()
    {
        return Collection::make(ResponsivCurrency::listEnabled());
    }

    public function convert(float $amount): float
    {
        if (! $this->needExchange()) {
            return $amount;
        }

        return ResponsivConverter::instance()
            ->convert($amount, $this->baseCurrency, $this->activeCurrency, null);
    }

    // public function convertBack(float $amount): float
    // {
    //     $inForeign = $this->convert($amount);

    //     return $amount / $inForeign;
    // }

    public function needExchange(): bool
    {
        return $this->getBaseCurrency() !== $this->getActiveCurrency();
    }
}
