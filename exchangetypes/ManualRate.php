<?php

namespace Empu\CurrencyUptick\ExchangeTypes;

use Responsiv\Currency\Classes\ExchangeBase;
use SystemException;
use Responsiv\Currency\Models\Currency;
use Responsiv\Currency\Models\ExchangeConverter;

class ManualRate extends ExchangeBase
{
    /**
     * {@inheritDoc}
     */
    public function converterDetails()
    {
        return [
            'name'        => 'Manual Input',
            'description' => 'Define exchange rates by yourself.'
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getExchangeRate($fromCurrency, $toCurrency)
    {
        $fromCurrency = trim(strtoupper($fromCurrency));
        $toCurrency = trim(strtoupper($toCurrency));
        $exchangeSlug = $this->exchangeSlug($fromCurrency, $toCurrency);

        $converterMeta = ExchangeConverter::where('class_name', self::class)->first();
        
        if (! $converterMeta || ! isset($converterMeta->{$exchangeSlug})) {
            throw new SystemException('Error, undefined exchange rate "'.$exchangeSlug.'"');
        }

        return $converterMeta->{$exchangeSlug};
    }

    /**
     * {@inheritDoc}
     */
    public function defineFormFields()
    {
        $fields = [];
        $primary = Currency::getPrimary();

        foreach (Currency::listAvailable() as $code => $name) {
            if ($primary->currency_code == $code) {
                continue;
            }

            $fields[$this->exchangeSlug($primary->currency_code, $code)] = [
                'label' => "{$primary->currency_code} to {$code}",
                'comment' => "Exchange rate from {$primary->name} to {$name}",
                'type' => 'number',
                'span' => 'strom',
                'cssClass' => 'col-md-6',
                'default' => 1,
                'tab' => 'Exchange Rates',
            ];
        }

        return compact('fields');
    }

    protected function exchangeSlug($fromCurrency, $toCurrency)
    {
        $fromCurrency = trim(strtoupper($fromCurrency));
        $toCurrency = trim(strtoupper($toCurrency));

        return "{$fromCurrency}_{$toCurrency}";
    }
}
