<?php namespace Empu\CurrencyUptick;

use Backend;
use Empu\CurrencyUptick\Facades\Currency;
use Money\Currencies\ISOCurrencies;
use Money\Money;
use System\Classes\PluginBase;
use Responsiv\Currency\Facades\Currency as CurrencyFacade;

/**
 * CurrencyUptick Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * @var array Plugin dependencies
     */
    public $require = [
        'Responsiv.Currency',
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Currency Uptick',
            'description' => 'Small increase functionalities for Responsiv.Currency',
            'author'      => 'Empu',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('currency.presenter', Classes\CurrencyPresenter::class);
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            Components\CurrencySwitcher::class => 'currencySwitcher',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'empu.currencyuptick.some_permission' => [
                'tab' => 'CurrencyUptick',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'currencyuptick' => [
                'label'       => 'CurrencyUptick',
                'url'         => Backend::url('empu/currencyuptick/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['empu.currencyuptick.*'],
                'order'       => 500,
            ],
        ];
    }

    /**
     * Register new Twig variables
     * @return array
     */
    public function registerMarkupTags()
    {
        return [
            'functions' => [
                // TODO: refactor to helper function
                'currency' => function ($amount, $useActive = true, $fancy = false) {
                    if ($amount instanceof Money) {
                        $currencies = new ISOCurrencies();
                        $subUnit = $currencies->subunitFor($amount->getCurrency());
                        $amount = $amount->getAmount() / pow(10, $subUnit);
                    }
                    
                    $baseCurrency = Currency::getBaseCurrency();
                    $toCurrency = $useActive ? Currency::getActiveCurrency() : $baseCurrency;
                    $format = ['in' => $baseCurrency];
                    $suffix = '';

                    if ($useActive && Currency::needExchange()) {
                        $format['to'] = $toCurrency;
                    }

                    if (\Str::upper($toCurrency) == 'IDR') {
                        $format['decimals'] = 0;
                    }

                    if ($fancy && $amount > 9999) {
                        $amount = $amount / 1000;
                        $suffix = 'K';
                    }
                    
                    return CurrencyFacade::format($amount, $format) . $suffix;
                }
            ]
        ];
    }

    public function registerCurrencyConverters()
    {
        return [
            ExchangeTypes\ManualRate::class => 'manual',
        ];
    }
}
