<?php

namespace Empu\CurrencyUptick\Components;

use Cms\Classes\ComponentBase;
use Empu\CurrencyUptick\Facades\Currency;
use Illuminate\Support\Facades\Request;

class CurrencySwitcher extends ComponentBase
{
    public $className;

    protected $currencyPresenter;

    public function componentDetails()
    {
        return [
            'name'        => 'Currency Switcher',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'className' => [
                'title'             => 'HTML elements class',
                'description'       => 'Class name for html element wrapper',
                'default'           => 'dropdown-menu',
                'type'              => 'string',
            ],
            'switchParamName' => [
                'title'             => 'Url Parameter',
                'description'       => 'Url parameter name for currency',
                'default'           => 'currency',
                'type'              => 'string',
            ],
            'urlAnchor' => [
                'title'             => 'Url Anchor',
                'description'       => 'Target anchor to scroll to',
                'type'              => 'string',
            ]
        ];
    }

    public function init()
    {
        $this->currencyPresenter = Currency::instance();
    }

    public function onRun()
    {
        $switchCurrency = request()->get('currency');

        if ($switchCurrency) {
            $this->currencyPresenter->setActiveCurrency($switchCurrency);
            $this->currencyPresenter->storeActiveCurrency();
        }

        $this->prepareVars();
    }

    protected function prepareVars()
    {
        $this->className = $this->property('className');
        $this->page['baseCurrency'] = $this->currencyPresenter
            ->getBaseCurrency();
        $this->page['activeCurrency'] = $this->currencyPresenter
            ->getActiveCurrency();
        $this->page['availableCurrencies'] = $this->currencyPresenter
            ->getForeignCurrencies()
            ->mapWithKeys(
                function ($name, $currencyCode) {
                    $queryParams = ['currency' => $currencyCode];
                    $url = Request::fullUrlWithQuery($queryParams);

                    return [$currencyCode => compact('name', 'url')];
                }
            );
    }
}
