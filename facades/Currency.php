<?php

namespace Empu\CurrencyUptick\Facades;

use October\Rain\Support\Facade;

class Currency extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'currency.presenter';
    }
}