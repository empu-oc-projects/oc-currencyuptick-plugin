<?php

use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Formatter\IntlMoneyFormatter;
use Money\Money;
use Responsiv\Currency\Models\Currency as ResponsivCurrency;

if (! function_exists('to_money')) {

    /**
     * Convert float to money
     * 
     * @param float $value
     * @param string $currencySymbol
     * @return \Money\Money
     */
    function to_money(float $value, string $currencySymbol = null): Money
    {
        $currencySymbol = $currencySymbol ?: ResponsivCurrency::getPrimary()->currency_code;
        $currency = new Currency($currencySymbol);
        $currencies = new ISOCurrencies();
        $amount = $value * pow(10, $currencies->subunitFor($currency));

        return new Money($amount, $currency);
    }
}

if (! function_exists('str_money')) {

    /**
     * Money presentation
     * 
     * @param \Money\Money $money
     * @param bool $currencyStyle
     * @return string
     */
    function str_money(?Money $money, bool $currencyStyle = true): ?string
    {
        if (! $money) {
            return null;
        }

        $currencies = new ISOCurrencies();
        $style = $currencyStyle ? NumberFormatter::CURRENCY : NumberFormatter::DECIMAL;
        $numberFormatter = new NumberFormatter('en_US', $style);

        if ($money->getCurrency()->getCode() == 'IDR') {
            $numberFormatter->setAttribute(NumberFormatter::FRACTION_DIGITS, 0);
        }
        
        $moneyFormatter = new IntlMoneyFormatter($numberFormatter, $currencies);
        return $moneyFormatter->format($money);
    }
}